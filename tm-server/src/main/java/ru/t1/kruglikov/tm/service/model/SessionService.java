package ru.t1.kruglikov.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.kruglikov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.kruglikov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.kruglikov.tm.api.repository.model.ISessionRepository;
import ru.t1.kruglikov.tm.api.service.IConnectionService;
import ru.t1.kruglikov.tm.api.service.model.ISessionService;
import ru.t1.kruglikov.tm.dto.model.SessionDTO;
import ru.t1.kruglikov.tm.enumerated.SessionSort;
import ru.t1.kruglikov.tm.exception.field.UserIdEmptyException;
import ru.t1.kruglikov.tm.model.Session;


import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    @Autowired
    private ISessionRepository repository;

    @Override
    @Transactional
    public void removeAll() {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAll(repository.findAll()
                .stream()
                .filter(x -> x.getUser().getId().equals(userId)).collect(Collectors.toList()));
    }

    @Nullable
    @Override
    public List<Session> findAll(@Nullable SessionSort sort){
        if (sort == null) return repository.findAll();
        return repository.findAll(sort.getColumnName());
    };

}