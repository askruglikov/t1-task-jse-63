package ru.t1.kruglikov.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class DataJsonLoadJaxBRequest extends AbstractUserRequest {

    public DataJsonLoadJaxBRequest(@Nullable String token) {
        super(token);
    }

}
