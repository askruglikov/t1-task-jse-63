package ru.t1.kruglikov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.kruglikov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.kruglikov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.kruglikov.tm.api.service.IPropertyService;
import ru.t1.kruglikov.tm.dto.request.data.*;
import ru.t1.kruglikov.tm.dto.request.user.UserLoginRequest;
import ru.t1.kruglikov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.kruglikov.tm.service.PropertyService;
import ru.t1.kruglikov.tm.marker.IntegrationCategory;

import static ru.t1.kruglikov.tm.constant.UserTestData.*;

@Category(IntegrationCategory.class)
public final class DomainEndpointTest {

    @Nullable
    private String adminToken;

    @Nullable
    private String userToken;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance(propertyService);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private String getToken(@NotNull final String login, @NotNull final String password) {
        @NotNull final UserLoginRequest request = new UserLoginRequest(null);
        request.setLogin(login);
        request.setPassword(password);

        @Nullable final String token = authEndpoint.login(request).getToken();
        return token;
    }

    @Before
    public void login() {
        adminToken = getToken(ADMIN_LOGIN, ADMIN_PASSWORD);
        userToken = getToken(USER_LOGIN, USER_PASSWORD);
    }

    @After
    public void logout() {
        authEndpoint.logout(new UserLogoutRequest(adminToken));
        authEndpoint.logout(new UserLogoutRequest(userToken));
        userToken = null;
        adminToken = null;
    }

    @Test
    public void testBackupSaveData() {
        @NotNull final DataBackupSaveRequest requestAdmin = new DataBackupSaveRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.backupSave(requestAdmin));

        @NotNull final DataBackupSaveRequest requestUser = new DataBackupSaveRequest(userToken);
        thrown.expect(Exception.class);
        domainEndpoint.backupSave(requestUser);
    }

    @Test
    public void testBackupLoadData() {
        @NotNull final DataBackupSaveRequest requestSave = new DataBackupSaveRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.backupSave(requestSave));

        @NotNull final DataBackupLoadRequest requestAdmin = new DataBackupLoadRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.backupLoad(requestAdmin));

        @NotNull final DataBackupLoadRequest requestUser = new DataBackupLoadRequest(userToken);
        thrown.expect(Exception.class);
        domainEndpoint.backupLoad(requestUser);
    }

    @Test
    public void testBase64SaveData() {
        @NotNull final DataBase64SaveRequest requestAdmin = new DataBase64SaveRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.base64Save(requestAdmin));

        @NotNull final DataBase64SaveRequest requestUser = new DataBase64SaveRequest(userToken);
        thrown.expect(Exception.class);
        domainEndpoint.base64Save(requestUser);
    }

    @Test
    public void testBase64LoadData() {
        @NotNull final DataBase64SaveRequest requestSave = new DataBase64SaveRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.base64Save(requestSave));

        @NotNull final DataBase64LoadRequest requestAdmin = new DataBase64LoadRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.base64Load(requestAdmin));

        @NotNull final DataBase64LoadRequest requestUser = new DataBase64LoadRequest(userToken);
        thrown.expect(Exception.class);
        domainEndpoint.base64Load(requestUser);
    }

    @Test
    public void testBinarySaveData() {
        @NotNull final DataBinarySaveRequest requestAdmin = new DataBinarySaveRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.binarySave(requestAdmin));

        @NotNull final DataBinarySaveRequest requestUser = new DataBinarySaveRequest(userToken);
        thrown.expect(Exception.class);
        domainEndpoint.binarySave(requestUser);
    }

    @Test
    public void testBinaryLoadData() {
        @NotNull final DataBinarySaveRequest requestSave = new DataBinarySaveRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.binarySave(requestSave));

        @NotNull final DataBinaryLoadRequest requestAdmin = new DataBinaryLoadRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.binaryLoad(requestAdmin));

        @NotNull final DataBinaryLoadRequest requestUser = new DataBinaryLoadRequest(userToken);
        thrown.expect(Exception.class);
        domainEndpoint.binaryLoad(requestUser);
    }

    @Test
    public void testJsonSaveFasterXmlData() {
        @NotNull final DataJsonSaveFasterXmlRequest requestAdmin = new DataJsonSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.jsonSaveFasterXml(requestAdmin));

        @NotNull final DataJsonSaveFasterXmlRequest requestUser = new DataJsonSaveFasterXmlRequest(userToken);
        thrown.expect(Exception.class);
        domainEndpoint.jsonSaveFasterXml(requestUser);
    }

    @Test
    public void testJsonLoadFasterXmlData() {
        @NotNull final DataJsonSaveFasterXmlRequest requestSave = new DataJsonSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.jsonSaveFasterXml(requestSave));

        @NotNull final DataJsonLoadFasterXmlRequest requestAdmin = new DataJsonLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.jsonLoadFasterXml(requestAdmin));

        @NotNull final DataJsonLoadFasterXmlRequest requestUser = new DataJsonLoadFasterXmlRequest(userToken);
        thrown.expect(Exception.class);
        domainEndpoint.jsonLoadFasterXml(requestUser);
    }

    @Test
    public void testJsonSaveJaxBData() {
        @NotNull final DataJsonSaveJaxBRequest requestAdmin = new DataJsonSaveJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.jsonSaveJaxB(requestAdmin));

        @NotNull final DataJsonSaveJaxBRequest requestUser = new DataJsonSaveJaxBRequest(userToken);
        thrown.expect(Exception.class);
        domainEndpoint.jsonSaveJaxB(requestUser);
    }

    @Test
    public void testJsonLoadJaxBData() {
        @NotNull final DataJsonSaveJaxBRequest requestSave = new DataJsonSaveJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.jsonSaveJaxB(requestSave));

        @NotNull final DataJsonLoadJaxBRequest requestAdmin = new DataJsonLoadJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.jsonLoadJaxB(requestAdmin));

        @NotNull final DataJsonLoadJaxBRequest requestUser = new DataJsonLoadJaxBRequest(userToken);
        thrown.expect(Exception.class);
        domainEndpoint.jsonLoadJaxB(requestUser);
    }

    @Test
    public void testXmlSaveFasterXmlData() {
        @NotNull final DataXmlSaveFasterXmlRequest requestAdmin = new DataXmlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.xmlSaveFasterXml(requestAdmin));

        @NotNull final DataXmlSaveFasterXmlRequest requestUser = new DataXmlSaveFasterXmlRequest(userToken);
        thrown.expect(Exception.class);
        domainEndpoint.xmlSaveFasterXml(requestUser);
    }

    @Test
    public void testXmlLoadFasterXmlData() {
        @NotNull final DataXmlSaveFasterXmlRequest requestSave = new DataXmlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.xmlSaveFasterXml(requestSave));

        @NotNull final DataXmlLoadFasterXmlRequest requestAdmin = new DataXmlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.xmlLoadFasterXml(requestAdmin));

        @NotNull final DataXmlLoadFasterXmlRequest requestUser = new DataXmlLoadFasterXmlRequest(userToken);
        thrown.expect(Exception.class);
        domainEndpoint.xmlLoadFasterXml(requestUser);
    }

    @Test
    public void testXmlSaveJaxBData() {
        @NotNull final DataXmlSaveJaxBRequest requestAdmin = new DataXmlSaveJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.xmlSaveJaxB(requestAdmin));

        @NotNull final DataXmlSaveJaxBRequest requestUser = new DataXmlSaveJaxBRequest(userToken);
        thrown.expect(Exception.class);
        domainEndpoint.xmlSaveJaxB(requestUser);
    }

    @Test
    public void testXmlLoadJaxBData() {
        @NotNull final DataXmlSaveJaxBRequest requestSave = new DataXmlSaveJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.xmlSaveJaxB(requestSave));

        @NotNull final DataXmlLoadJaxBRequest requestAdmin = new DataXmlLoadJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.xmlLoadJaxB(requestAdmin));

        @NotNull final DataXmlLoadJaxBRequest requestUser = new DataXmlLoadJaxBRequest(userToken);
        thrown.expect(Exception.class);
        domainEndpoint.xmlLoadJaxB(requestUser);
    }

    @Test
    public void testYamlSaveFasterXmlData() {
        @NotNull final DataYamlSaveFasterXmlRequest requestAdmin = new DataYamlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.yamlSaveFasterXml(requestAdmin));

        @NotNull final DataYamlSaveFasterXmlRequest requestUser = new DataYamlSaveFasterXmlRequest(userToken);
        thrown.expect(Exception.class);
        domainEndpoint.yamlSaveFasterXml(requestUser);
    }

    @Test
    public void testYamlLoadFasterXmlData() {
        @NotNull final DataYamlSaveFasterXmlRequest requestSave = new DataYamlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.yamlSaveFasterXml(requestSave));

        @NotNull final DataYamlLoadFasterXmlRequest requestAdmin = new DataYamlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.yamlLoadFasterXml(requestAdmin));

        @NotNull final DataYamlLoadFasterXmlRequest requestUser = new DataYamlLoadFasterXmlRequest(userToken);
        thrown.expect(Exception.class);
        domainEndpoint.yamlLoadFasterXml(requestUser);
    }

}
