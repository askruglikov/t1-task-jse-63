package ru.t1.kruglikov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.kruglikov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.kruglikov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.kruglikov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.kruglikov.tm.api.service.IPropertyService;
import ru.t1.kruglikov.tm.dto.model.TaskDTO;
import ru.t1.kruglikov.tm.dto.request.project.ProjectClearRequest;
import ru.t1.kruglikov.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.kruglikov.tm.dto.request.task.*;
import ru.t1.kruglikov.tm.dto.request.user.UserLoginRequest;
import ru.t1.kruglikov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.kruglikov.tm.dto.response.task.*;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.model.Task;
import ru.t1.kruglikov.tm.service.PropertyService;
import ru.t1.kruglikov.tm.marker.IntegrationCategory;

import java.util.List;

import static ru.t1.kruglikov.tm.constant.ProjectTestData.PROJECT1_DESC;
import static ru.t1.kruglikov.tm.constant.ProjectTestData.PROJECT1_NAME;
import static ru.t1.kruglikov.tm.constant.TaskTestData.*;
import static ru.t1.kruglikov.tm.constant.UserTestData.*;
import static ru.t1.kruglikov.tm.constant.UserTestData.USER_PASSWORD;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest {

    @Nullable
    private String adminToken;

    @Nullable
    private String userToken;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private String getToken(@NotNull final String login, @NotNull final String password) {
        @NotNull final UserLoginRequest request = new UserLoginRequest(null);
        request.setLogin(login);
        request.setPassword(password);

        @Nullable final String token = authEndpoint.login(request).getToken();
        return token;
    }

    @Before
    public void login() {
        adminToken = getToken(ADMIN_LOGIN, ADMIN_PASSWORD);
        userToken = getToken(USER_LOGIN, USER_PASSWORD);
        taskEndpoint.clear(new TaskClearRequest(adminToken));
        taskEndpoint.clear(new TaskClearRequest(userToken));
        projectEndpoint.clear(new ProjectClearRequest(adminToken));
        projectEndpoint.clear(new ProjectClearRequest(userToken));
    }

    @After
    public void logout() {
        authEndpoint.logout(new UserLogoutRequest(adminToken));
        authEndpoint.logout(new UserLogoutRequest(userToken));
        userToken = null;
        adminToken = null;
    }

    @Test
    public void testcreate() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);

        @NotNull final TaskCreateResponse responseCreate = taskEndpoint.create(requestCreate);
        @Nullable TaskDTO taskCreate = responseCreate.getTask();
        Assert.assertNotNull(taskCreate);
        Assert.assertEquals(TASK1_NAME, taskCreate.getName());
        Assert.assertEquals(TASK1_DESC, taskCreate.getDescription());

        @NotNull final TaskListRequest requestListUser = new TaskListRequest(userToken);
        @NotNull final TaskListResponse responseListUser = taskEndpoint.list(requestListUser);
        @Nullable final List<TaskDTO> taskListUser = responseListUser.getTasks();
        Assert.assertNotNull(taskListUser);
        Assert.assertEquals(1, taskListUser.size());
        Assert.assertEquals(TASK1_NAME, taskListUser.get(0).getName());
        Assert.assertEquals(TASK1_DESC, taskListUser.get(0).getDescription());

        @NotNull final TaskListRequest requestListAdmin = new TaskListRequest(adminToken);
        @NotNull final TaskListResponse responseListAdmin = taskEndpoint.list(requestListAdmin);
        @Nullable final List<TaskDTO> taskListAdmin = responseListAdmin.getTasks();
        Assert.assertNull(taskListAdmin);

        @NotNull final TaskCreateRequest requestException = new TaskCreateRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.create(requestException);
    }

    @Test
    public void testlist() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK3_NAME);
        requestCreate.setDescription(TASK3_DESC);
        taskEndpoint.create(requestCreate);

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.list(requestList);
        @Nullable List<TaskDTO> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(3, taskList.size());
        Assert.assertEquals(TASK1_NAME, taskList.get(0).getName());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(TASK3_NAME, taskList.get(2).getName());
        Assert.assertEquals(TASK1_DESC, taskList.get(0).getDescription());
        Assert.assertEquals(TASK2_DESC, taskList.get(1).getDescription());
        Assert.assertEquals(TASK3_DESC, taskList.get(2).getDescription());
    }

    @Test
    public void testshowById() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        @NotNull final String taskId = taskEndpoint.create(requestCreate).getTask().getId();

        requestCreate.setName(TASK3_NAME);
        requestCreate.setDescription(TASK3_DESC);
        taskEndpoint.create(requestCreate);

        @NotNull final TaskShowByIdRequest requestShow = new TaskShowByIdRequest(userToken);
        requestShow.setTaskId(taskId);
        @NotNull final TaskShowByIdResponse responseShow = taskEndpoint.showById(requestShow);
        @Nullable final TaskDTO task = responseShow.getTask();
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(TASK2_DESC, task.getDescription());

        @NotNull final TaskShowByIdRequest requestException = new TaskShowByIdRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.showById(requestException);
    }

    @Test
    public void testshowByIndex() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK3_NAME);
        requestCreate.setDescription(TASK3_DESC);
        taskEndpoint.create(requestCreate);

        @NotNull final TaskShowByIndexRequest requestShow = new TaskShowByIndexRequest(userToken);
        requestShow.setIndex(1);
        @NotNull final TaskShowByIndexResponse responseShow = taskEndpoint.showByIndex(requestShow);
        @Nullable final TaskDTO task = responseShow.getTask();
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(TASK2_DESC, task.getDescription());

        @NotNull final TaskShowByIndexRequest requestException = new TaskShowByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        taskEndpoint.showByIndex(requestException);
    }

    @Test
    public void testclear() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK3_NAME);
        requestCreate.setDescription(TASK3_DESC);
        taskEndpoint.create(requestCreate);

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.list(requestList);
        @Nullable List<TaskDTO> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(3, taskList.size());

        @NotNull final TaskClearRequest requestClear = new TaskClearRequest(userToken);
        taskEndpoint.clear(requestClear);

        responseList = taskEndpoint.list(requestList);
        taskList = responseList.getTasks();
        Assert.assertNull(taskList);
    }

    @Test
    public void testremoveById() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK3_NAME);
        requestCreate.setDescription(TASK3_DESC);
        taskEndpoint.create(requestCreate);

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.list(requestList);
        @Nullable List<TaskDTO> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(3, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        @NotNull final String projectId = taskList.get(1).getId();

        @NotNull final TaskRemoveByIdRequest requestRemove = new TaskRemoveByIdRequest(userToken);
        requestRemove.setTaskId(projectId);
        taskEndpoint.removeById(requestRemove);

        responseList = taskEndpoint.list(requestList);
        taskList = responseList.getTasks();
        Assert.assertEquals(2, taskList.size());
        Assert.assertNotEquals(TASK2_NAME, taskList.get(1).getName());

        @NotNull final TaskRemoveByIdRequest requestException = new TaskRemoveByIdRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.removeById(requestException);
    }

    @Test
    public void testremoveByIndex() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK3_NAME);
        requestCreate.setDescription(TASK3_DESC);
        taskEndpoint.create(requestCreate);

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.list(requestList);
        @Nullable List<TaskDTO> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(3, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        @NotNull final String taskId = taskList.get(1).getId();

        @NotNull final TaskRemoveByIndexRequest requestRemove = new TaskRemoveByIndexRequest(userToken);
        requestRemove.setIndex(1);
        taskEndpoint.removeByIndex(requestRemove);

        responseList = taskEndpoint.list(requestList);
        taskList = responseList.getTasks();
        Assert.assertEquals(2, taskList.size());
        Assert.assertNotEquals(TASK2_NAME, taskList.get(1).getName());

        @NotNull final TaskRemoveByIndexRequest requestException = new TaskRemoveByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        taskEndpoint.removeByIndex(requestException);
    }

    @Test
    public void testupdateById() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        @NotNull final String taskId = taskEndpoint.create(requestCreate).getTask().getId();

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.list(requestList);
        @Nullable List<TaskDTO> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(TASK2_DESC, taskList.get(1).getDescription());

        @NotNull final TaskUpdateByIdRequest requestUpdate = new TaskUpdateByIdRequest(userToken);
        requestUpdate.setTaskId(taskId);
        requestUpdate.setName(TASK3_NAME);
        requestUpdate.setDescription(TASK3_DESC);
        @Nullable TaskDTO task = taskEndpoint.updateById(requestUpdate).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK3_NAME, task.getName());
        Assert.assertEquals(TASK3_DESC, task.getDescription());

        responseList = taskEndpoint.list(requestList);
        taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK3_NAME, taskList.get(1).getName());
        Assert.assertEquals(TASK3_DESC, taskList.get(1).getDescription());

        @NotNull final TaskUpdateByIdRequest requestException = new TaskUpdateByIdRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.updateById(requestException);
    }

    @Test
    public void testupdateByIndex() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.create(requestCreate).getTask().getId();

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.list(requestList);
        @Nullable List<TaskDTO> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(TASK2_DESC, taskList.get(1).getDescription());

        @NotNull final TaskUpdateByIndexRequest requestUpdate = new TaskUpdateByIndexRequest(userToken);
        requestUpdate.setIndex(1);
        requestUpdate.setName(TASK3_NAME);
        requestUpdate.setDescription(TASK3_DESC);
        @Nullable TaskDTO task = taskEndpoint.updateByIndex(requestUpdate).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK3_NAME, task.getName());
        Assert.assertEquals(TASK3_DESC, task.getDescription());

        responseList = taskEndpoint.list(requestList);
        taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK3_NAME, taskList.get(1).getName());
        Assert.assertEquals(TASK3_DESC, taskList.get(1).getDescription());

        @NotNull final TaskUpdateByIndexRequest requestException = new TaskUpdateByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        taskEndpoint.updateByIndex(requestException);
    }

    @Test
    public void testchangeStatusById() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        @NotNull final String taskId = taskEndpoint.create(requestCreate).getTask().getId();

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.list(requestList);
        @Nullable List<TaskDTO> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, taskList.get(1).getStatus());

        @NotNull final TaskChangeStatusByIdRequest requestChangeStatus = new TaskChangeStatusByIdRequest(userToken);
        requestChangeStatus.setTaskId(taskId);
        requestChangeStatus.setStatus(Status.IN_PROGRESS);
        @Nullable TaskDTO task = taskEndpoint.changeStatusById(requestChangeStatus).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());

        responseList = taskEndpoint.list(requestList);
        taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.IN_PROGRESS, taskList.get(1).getStatus());

        @NotNull final TaskChangeStatusByIdRequest requestException = new TaskChangeStatusByIdRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.changeStatusById(requestException);
    }

    @Test
    public void testchangeStatusByIndex() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.create(requestCreate).getTask().getId();

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.list(requestList);
        @Nullable List<TaskDTO> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, taskList.get(1).getStatus());

        @NotNull final TaskChangeStatusByIndexRequest requestChangeStatus = new TaskChangeStatusByIndexRequest(userToken);
        requestChangeStatus.setIndex(1);
        requestChangeStatus.setStatus(Status.IN_PROGRESS);
        @Nullable TaskDTO task = taskEndpoint.changeStatusByIndex(requestChangeStatus).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());

        responseList = taskEndpoint.list(requestList);
        taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.IN_PROGRESS, taskList.get(1).getStatus());

        @NotNull final TaskChangeStatusByIndexRequest requestException = new TaskChangeStatusByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        taskEndpoint.changeStatusByIndex(requestException);
    }

    @Test
    public void testcompleteById() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        @NotNull final String taskId = taskEndpoint.create(requestCreate).getTask().getId();

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.list(requestList);
        @Nullable List<TaskDTO> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, taskList.get(1).getStatus());

        @NotNull final TaskCompleteByIdRequest requestChangeStatus = new TaskCompleteByIdRequest(userToken);
        requestChangeStatus.setTaskId(taskId);
        @Nullable TaskDTO task = taskEndpoint.completeById(requestChangeStatus).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());

        responseList = taskEndpoint.list(requestList);
        taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.COMPLETED, taskList.get(1).getStatus());

        @NotNull final TaskCompleteByIdRequest requestException = new TaskCompleteByIdRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.completeById(requestException);
    }

    @Test
    public void testcompleteByIndex() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.create(requestCreate).getTask().getId();

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.list(requestList);
        @Nullable List<TaskDTO> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, taskList.get(1).getStatus());

        @NotNull final TaskCompleteByIndexRequest requestChangeStatus = new TaskCompleteByIndexRequest(userToken);
        requestChangeStatus.setIndex(1);
        @Nullable TaskDTO task = taskEndpoint.completeByIndex(requestChangeStatus).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());

        responseList = taskEndpoint.list(requestList);
        taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.COMPLETED, taskList.get(1).getStatus());

        @NotNull final TaskCompleteByIndexRequest requestException = new TaskCompleteByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        taskEndpoint.completeByIndex(requestException);
    }

    @Test
    public void teststartById() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        @NotNull final String taskId = taskEndpoint.create(requestCreate).getTask().getId();

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.list(requestList);
        @Nullable List<TaskDTO> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, taskList.get(1).getStatus());

        @NotNull final TaskStartByIdRequest requestChangeStatus = new TaskStartByIdRequest(userToken);
        requestChangeStatus.setTaskId(taskId);
        @Nullable TaskDTO task = taskEndpoint.startById(requestChangeStatus).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());

        responseList = taskEndpoint.list(requestList);
        taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.IN_PROGRESS, taskList.get(1).getStatus());

        @NotNull final TaskStartByIdRequest requestException = new TaskStartByIdRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.startById(requestException);
    }

    @Test
    public void teststartByIndex() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.create(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.create(requestCreate).getTask().getId();

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.list(requestList);
        @Nullable List<TaskDTO> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, taskList.get(1).getStatus());

        @NotNull final TaskStartByIndexRequest requestChangeStatus = new TaskStartByIndexRequest(userToken);
        requestChangeStatus.setIndex(1);
        @Nullable TaskDTO task = taskEndpoint.startByIndex(requestChangeStatus).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());

        responseList = taskEndpoint.list(requestList);
        taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.IN_PROGRESS, taskList.get(1).getStatus());

        @NotNull final TaskStartByIndexRequest requestException = new TaskStartByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        taskEndpoint.startByIndex(requestException);
    }

    @Test
    public void testBindTaskToProject() {
        @NotNull final TaskCreateRequest requestcreate = new TaskCreateRequest(userToken);
        requestcreate.setName(TASK1_NAME);
        requestcreate.setDescription(TASK1_DESC);
        taskEndpoint.create(requestcreate);

        requestcreate.setName(TASK2_NAME);
        requestcreate.setDescription(TASK2_DESC);
        @NotNull final String taskId = taskEndpoint.create(requestcreate).getTask().getId();

        @NotNull final ProjectCreateRequest request_create = new ProjectCreateRequest(userToken);
        request_create.setName(PROJECT1_NAME);
        request_create.setDescription(PROJECT1_DESC);
        @NotNull final String projectId = projectEndpoint.create(request_create).getProject().getId();

        @NotNull final TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(userToken);
        requestBind.setProjectId(projectId);
        requestBind.setTaskId(taskId);
        taskEndpoint.bindToProject(requestBind);

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.list(requestList);
        @Nullable List<TaskDTO> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(projectId, taskList.get(1).getProjectId());

        @NotNull final TaskBindToProjectRequest requestException = new TaskBindToProjectRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.bindToProject(requestException);
    }

    @Test
    public void testUnbindTaskFromProject() {
        @NotNull final TaskCreateRequest requestcreate = new TaskCreateRequest(userToken);
        requestcreate.setName(TASK1_NAME);
        requestcreate.setDescription(TASK1_DESC);
        taskEndpoint.create(requestcreate);

        requestcreate.setName(TASK2_NAME);
        requestcreate.setDescription(TASK2_DESC);
        @NotNull final String taskId = taskEndpoint.create(requestcreate).getTask().getId();

        @NotNull final ProjectCreateRequest request_create = new ProjectCreateRequest(userToken);
        request_create.setName(PROJECT1_NAME);
        request_create.setDescription(PROJECT1_DESC);
        @NotNull final String projectId = projectEndpoint.create(request_create).getProject().getId();

        @NotNull final TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(userToken);
        requestBind.setProjectId(projectId);
        requestBind.setTaskId(taskId);
        taskEndpoint.bindToProject(requestBind);

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.list(requestList);
        @Nullable List<TaskDTO> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(projectId, taskList.get(1).getProjectId());

        @NotNull final TaskUnbindFromProjectRequest requestUnbind = new TaskUnbindFromProjectRequest(userToken);
        requestUnbind.setProjectId(projectId);
        requestUnbind.setTaskId(taskId);
        taskEndpoint.unbindFromProject(requestUnbind);

        responseList = taskEndpoint.list(requestList);
        taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(null, taskList.get(1).getProjectId());

        @NotNull final TaskUnbindFromProjectRequest requestException = new TaskUnbindFromProjectRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.unbindFromProject(requestException);
    }

    @Test
    public void testlistByProjectId() {
        @NotNull final TaskCreateRequest requestcreate = new TaskCreateRequest(userToken);
        requestcreate.setName(TASK1_NAME);
        requestcreate.setDescription(TASK1_DESC);
        taskEndpoint.create(requestcreate);

        requestcreate.setName(TASK2_NAME);
        requestcreate.setDescription(TASK2_DESC);
        @NotNull final String taskId = taskEndpoint.create(requestcreate).getTask().getId();

        @NotNull final ProjectCreateRequest request_create = new ProjectCreateRequest(userToken);
        request_create.setName(PROJECT1_NAME);
        request_create.setDescription(PROJECT1_DESC);
        @NotNull final String projectId = projectEndpoint.create(request_create).getProject().getId();

        @NotNull final TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(userToken);
        requestBind.setProjectId(projectId);
        requestBind.setTaskId(taskId);
        taskEndpoint.bindToProject(requestBind);

        @NotNull final TaskListByProjectIdRequest requestList = new TaskListByProjectIdRequest(userToken);
        requestList.setProjectId(projectId);
        @NotNull TaskListByProjectIdResponse responseList = taskEndpoint.listByProjectId(requestList);
        @Nullable List<TaskDTO> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(1, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(0).getName());
        Assert.assertEquals(projectId, taskList.get(0).getProjectId());

        @NotNull final TaskListByProjectIdRequest requestException = new TaskListByProjectIdRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.listByProjectId(requestException);
    }

}
