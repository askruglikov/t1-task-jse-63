package ru.t1.kruglikov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.kruglikov.tm.event.ConsoleEvent;
import ru.t1.kruglikov.tm.util.TerminalUtil;

@Component
public final class ProjectCreateListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-create";

    @NotNull
    public static final String DESCRIPTION = "Create new project.";

    @Override
    @EventListener(condition = "@projectCreateListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[PROJECT CREATE]");

        System.out.println("Enter project name:");
        @Nullable final String name = TerminalUtil.nextLine();

        System.out.println("Enter project description:");
        @Nullable final String description = TerminalUtil.nextLine();

        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        projectEndpoint.create(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
